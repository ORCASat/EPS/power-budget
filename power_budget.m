% This script can be used to get a close aproximation of a power budget
%One just need to change the following inputs:
%       1 * .csv file with power generated from STK +output+
%       2 * time step (in seconds) used in the STK simulation +d+
%       3 * orbit period +tau+
%       4 * battery capacity (in Wh) +cap+
%       5 * battery charging efficiency +eff+
%       6 * ADCS duty cycle and it's consumption +dc+
%       7 * Bus consumption +bus+
%       8 * Payload consumption +ac+ and +cc+
%It's recommended to run all power simulations in STK and save the reports
%as .csv files. Even if the team can't get a STK Pro License, it's possible
%to get a Pro License for 15 days. That way would be possible to run all
%desired scenarios and present the results with this script
%
%The power consummed by the components is modulated as pulses. For example,
%the CHIME is assumed to be accessed 3 times in a day, so the frequency is
%based on that. The pulse width is based on the average access time for
%each ground station. 

function power_budget()

clc
clear
close all

%%
%**************************************************************
%*                        POWER DATA                          *
%**************************************************************

%Power Generation
gen_eff = 0.8;
output = xlsread('sim_data.xlsx');
power = gen_eff*output(:,1);        %Array with power generation values
d = 60;                             %Time increment in seconds
tf = size(power,1);                 %Final time step

%Energy Storage
cap = 32;                           %Battery capacity in Whr
bat_chg_eff = 0.98;                 %Battery charging efficiency
bat_dchg_eff = 0.98;                %Battery discharging efficiency

capacity = zeros(1,tf);
capacity(1) = cap;


%%
%**************************************************************
%*                       COMPONENTS DATA                      *
%**************************************************************

tau = 5580;                 %Orbit Period in seconds
o = ceil(86400/tau);        %Orbits in a day

%Systems' frequency ******************************************
%CHANGE the numerator to the number of times it's used per day
%In this case the comms system is treated as a payload

fu = (6/(o*tau));           %UVic freq (Altair + Comms)
fc = (6/(o*tau));           %CHIME freq (Comms)
fa = (1/(o*tau));           %Altair freq
fgps = 1/86400;             %GPS freq

%System's pulse duration in seconds **************************
wp = 5;                     %ALTAIR payload duration based on required access time, in seconds
wchime = 180;               %CHIME payload duraction based on required access time, in seconds
wgps = 60;                  %GPS operation duraction based on required time to get update, in seconds

%Time arrays *************************************************
t = 0:d:(d*tf-1*d);                     %Total time array

tu = 0+200*wp*6:(1/fu):d*tf;            %UVic time array
tc = 0+200*2*wchime:(1/fc):d*tf;        %CHIME time array 

lsst_time = 0+200*wp:(1/fa):d*tf;       %LSST time array
pan_time = 0+200*wp:(1/fa):d*tf;        %Pan-STARRS time array

gps_time = 0+100*wgps:(1/fgps):d*tf;    %GPS time array

%Systems' consumption ****************************************
pol_eff = 0.8;                      %POL converter efficiency (end-to-end)  

eps_pwr = 0.1/pol_eff;              % EPS consumption at 100% duty cycle
obc_pwr = 0.2/pol_eff;              % OBC consumption at 100% duty cycle
adcs_pwr = 1.0/pol_eff;             %ADCS consumption
comms_rx_pwr = 0.1/pol_eff;         % COMMS RX at 100% duty cycle

altair_pwr = 1.0/pol_eff;           %ALTAIR consumption
comms_tx_pwr = 3.3/pol_eff;         %COMMS consumption
gps_pwr = 0.8/pol_eff;              %GPS power consumption

%Systems' pulses *********************************************
%eso = ac*pulstran(t,te+(21780/(d*10)),'rectpuls',wp);
lsst = altair_pwr*pulstran(t,lsst_time+(25380/d),'rectpuls',wp);
pan = altair_pwr*pulstran(t,pan_time+(36180/(d*20)),'rectpuls',wp);

uvic = (altair_pwr + comms_tx_pwr) * pulstran(t,tu,'rectpuls',wp);
%adcs = adcs_pwr*pulstran(t,adcs_time+190,'rectpuls',wadcs);
chime = comms_tx_pwr*pulstran(t,tc+(180/d),'rectpuls',wp);

gps = gps_pwr*pulstran(t,gps_time+(300/(1*d)),'rectpuls',wgps);

%%
%**************************************************************
%*                        CONSUMPTION                         *
%**************************************************************

a = zeros(1,tf);                 %Dummy array for power balance
cons = zeros(1,tf) + eps_pwr + obc_pwr + comms_rx_pwr + adcs_pwr + gps + lsst + pan + uvic + chime;

for i = 1:tf
   
    if((power(i) - cons(i)) >= 0)
        a(i) = a(i) + (power(i) - cons(i))*bat_chg_eff;
    else
        a(i) = a(i) + (power(i) - cons(i))/bat_dchg_eff;
    end
    
end

for i =2:tf
   
    capacity(i) = capacity(i-1) + d*a(i)/3600;
    
    if (capacity(i) > cap)
        capacity(i) = cap;
    elseif (capacity(i) < 0)
        capacity(i) = 0;
    end
    
end

%%
%**************************************************************
%*                           PLOTS                            *
%**************************************************************
day = 86400;
%Battery capacity
figure
set(figure,'defaultAxesColorOrder',[[0 0 0]; [0 0 0 ]]);

hold on
%yyaxis left
%plot(t/day,power,'b','LineWidth',1.5)
%s = plot(t/day,cons,'r-','LineWidth',1.5);
% plot(t/day,gps,'k-','LineWidth',2.0)
%ylabel('Power, W')
%s.Color(4) = 0.25;
%ylim([0 max(power)+1])

plot(t/day,100*capacity/cap,'g','LineWidth',1.5)
xlabel('time, days')
ylabel('Battery state, %')
%xlim([0 floor(tf*d/day)])
%ylim([0 100])

title('Battery State of Charge (SoC)')

fprintf('Maximum DoD: %.2f%%',100 - min(100*capacity/cap))

mean(power);
max(power);


%%
%Average Daily Power Generation  
% n_days = floor(tf*d/day);                   %Number of days in the simulation
% d_dayy = day/d;                             %Time increments in a day
% v = 1;                                      %Day delimiter
% day_average = zeros(1,n_days);              %Average power generated in a day
% 
% for i = 1:n_days
%     
%     d_day = d_dayy * i;
%     day_average (i) = day_average(i) + mean(power(v:d_day));
%     v = d_day + 1;
%     
% end
% % size(tl',1);
% days = 1:1:n_days;
% 
% dim = [0.715 0.0001 0.3 0.3];
% str = {['Average Available Power: ', num2str(mean(day_average)),' W']
%        ['Average Consumption: ', num2str(sum(cons(1:ceil(o/d)))),' W']
%        ['Payload Duty Cycle: ', num2str(100*ceil((wp/tau)*1000)/1000),'%']
%        ['Comms Duty Cycle: ', num2str(100*ceil((wchime/tau)*1000)/1000),'%']};

% figure
% plot(days,day_average,'LineWidth',1.5)
% hold on
% plot(days,mean(day_average)*ones(n_days),'g','LineWidth',1.5)
% %plot(days,mean(cons)*ones(n_days),'g','LineWidth',1.5)
% xlabel('time, days')
% ylabel('Average Available Power, W')
% title('Average Power Generation per Day')
% legend('Average Available Power per day','Average Available Power for Year')
% annotation('textbox',dim,'String',str,'FitBoxToText','on');
% xlim([1 n_days])
% ylim([0 ceil(max(day_average))+1])
% 
% mean(day_average);
% mean(cons);

%%
%Average power generation per orbit

n_orbs = floor(tf*d/tau);                   %Number of orbits in the simulation
d_orbss = tau/d;                            %Time increments in an orbit
vo = 1;                                     %Orbit delimiter
orbit_average = zeros(1,n_orbs);            %Average power generated in an orbit

for i = 1:n_orbs
    
    d_orbs = d_orbss * i;
    orbit_average (i) = orbit_average(i) + mean(power(vo:d_orbs));
    vo = d_orbs + 1;
    
end

orbs = 1:1:n_orbs;

dim = [0.715 0.0001 0.3 0.3];
str = {['Average Available Power: ', num2str(mean(orbit_average)),' W']
       ['Average Consumption: ', num2str(sum(cons(1:ceil(o/d)))),' W']
       ['Payload Duty Cycle: ', num2str(100*ceil((wp/tau)*1000)/1000),'%']
       ['Comms Duty Cycle: ', num2str(100*ceil((wchime/tau)*1000)/1000),'%']};

figure

plot(orbs,orbit_average, 'b')
hold on

plot(mean(orbit_average)*ones(n_orbs), 'g')
hold on

plot(sum(cons(1:ceil(o/d)))*ones(n_orbs), 'r')


% hold on
% plot(orbs,mean(orbit_average)*ones(n_orbs),'g','LineWidth',1.5)
% 
% hold on
% plot(orbs,sum(cons(1:ceil(o/d)))*ones(n_orbs),'r','LineWidth',1.5)

%plot(days,mean(cons)*ones(n_days),'g','LineWidth',1.5)
xlabel('time, orbits')
ylabel('Average Available Power, W')
title('Average Power Generated per Orbit')

legend('Average Available Power per Orbit','Average Available Power', 'Average Power Consumed')

annotation('textbox',dim,'String',str,'FitBoxToText','on');
xlim([1 n_orbs])
ylim([0 ceil(max(orbit_average))+1])

mean(orbit_average);
mean(cons);
end